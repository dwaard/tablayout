package examples.quickprogrammingtips.com.tablayout;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;//must be changed; Android Studio makes it Fragment!?
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

//test line

public class PlayFragment extends Fragment {
    private DataPassListener listener;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Make sure that container activity implement the callback interface
        if (activity instanceof DataPassListener){
            Log.v("data", "listener");
        }
        listener = (DataPassListener) activity;
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View view =  inflater.inflate(R.layout.fragment_play, container, false);

            // Suppose that when a button clicked second FragmentB will be inflated
            // some data on FragmentA will pass FragmentB
            Button passDataButton = (Button) view.findViewById(R.id.passDataButton);
            passDataButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getId() == R.id.passDataButton) {
                        if(listener != null) // important to prevent NullPointerException
                            listener.dataPassed(new TwoStrings("ad", "bad"));

                    }
                }
            });
            return view;
        }
    }
