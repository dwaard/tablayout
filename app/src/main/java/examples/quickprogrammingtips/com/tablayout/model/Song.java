package examples.quickprogrammingtips.com.tablayout.model;

/**
 * Created by anton on 8-1-16.
 */
public class Song {
    private int length;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
